import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent}from'./components/home/home.component';
import {CanvasComponent}from'./components/canvas/canvas.component';

const appRoutes:Routes = [

    { path: '', component: HomeComponent },
      { path: 'canvas', component: CanvasComponent },
]; 


export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})
export class CanvasComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    var scalableIframe = document.getElementById('scalable-frame');
    var containColumn = document.getElementById('canvas-contain-column');

    var parentWidth = scalableIframe.parentElement.clientWidth;
    console.log(parentWidth);
    var frameWidth = scalableIframe.clientWidth;
    var frameHeight = scalableIframe.clientHeight;
    console.log(frameWidth);

    var scale = parentWidth / frameWidth;
    console.log(scale);

    var columnHeight = frameHeight * scale;
    console.log(columnHeight);


    scalableIframe.style.setProperty(`--zoom-ratio`, scale+"");
    containColumn.style.height = `${columnHeight}px`;

    console.log();

  }


  zoomInCanvas() {

    console.log('zooming in');

    var zoomRatio = document.getElementById('scalable-frame').style.getPropertyValue('--zoom-ratio');

    console.log(zoomRatio);
    console.log(typeof (zoomRatio));
    var zoomRatioFloat = parseFloat(zoomRatio);
    console.log(zoomRatioFloat);
    zoomRatioFloat = zoomRatioFloat + 0.1;

    console.log(zoomRatioFloat);

    document.getElementById('scalable-frame').style.setProperty(`--zoom-ratio`, zoomRatioFloat + '');
  }

  zoomOutCanvas() {

      var zoomRatio = document.getElementById('scalable-frame').style.getPropertyValue('--zoom-ratio');

    console.log(zoomRatio);
    console.log(typeof (zoomRatio));
    var zoomRatioFloat = parseFloat(zoomRatio);
    console.log(zoomRatioFloat);
    zoomRatioFloat = zoomRatioFloat - 0.1;

    console.log(zoomRatioFloat);
    


    document.getElementById('scalable-frame').style.setProperty(`--zoom-ratio`, zoomRatioFloat+'');

  }
}

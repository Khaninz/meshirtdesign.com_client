import { MeShirtDesignPage } from './app.po';

describe('me-shirt-design App', function() {
  let page: MeShirtDesignPage;

  beforeEach(() => {
    page = new MeShirtDesignPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
